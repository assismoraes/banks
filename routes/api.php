<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user()->userType()->first();
});

Route::group(['prefix' => 'common_user', 'middleware' => ['auth:api', 'role:common_user']], function() {
    Route::post('/deposit_to_any_account', 'AccountController@deposit_to_any_account');
    Route::post('/cash_out', 'AccountController@cash_out');
    Route::post('/transfer', 'AccountController@transfer');
    Route::get('/transactions/{month}/{year}', 'AccountController@transactions');
    Route::get('/current_balance', 'AccountController@current_balance');
});

Route::group(['prefix' => 'admin_user', 'middleware' => ['auth:api', 'role:admin_user']], function() {
    Route::post('/create_account', 'AccountController@create_account');
});

Route::group(['prefix' => 'master_user', 'middleware' => ['auth:api', 'role:master_user']], function() {
    Route::post('/create_bank', 'BankController@create_bank');
    Route::put('/edit_bank', 'BankController@edit_bank');
    Route::get('/list_banks', 'BankController@list_banks');
    Route::delete('/delete_bank/{id}', 'BankController@delete_bank');

    Route::post('/create_branch', 'BranchController@create_branch');
    Route::put('/edit_branch', 'BranchController@edit_branch');
    Route::get('/list_branches', 'BranchController@list_branches');
    Route::get('/list_branches_by_bank/{bankId}', 'BranchController@list_branches_by_bank');
    Route::delete('/delete_branch/{id}', 'BranchController@delete_branch');

    Route::post('/create_admin', 'UserController@create_admin');
    Route::put('/edit_admin', 'UserController@edit_admin');
    Route::get('/list_admins', 'UserController@list_admins');
    Route::delete('/delete_admin/{id}', 'UserController@delete_admin');
    Route::get('/find_admin/{search?}', 'UserController@find_admin');
});

