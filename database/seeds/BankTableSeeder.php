<?php

use Illuminate\Database\Seeder;
use App\Models\Bank;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bank::create(['name' => 'Banco do Brasil']);
        Bank::create(['name' => 'HSBC']);
        Bank::create(['name' => 'Itaú']);
        Bank::create(['name' => 'Santander']);
        Bank::create(['name' => 'Caixa']);
    }
}
