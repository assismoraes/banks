<?php

use Illuminate\Database\Seeder;
use App\Models\Person;

class PersonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Person::create(['registry_number' => '7746', 'name' => 'Common User 1']);
        Person::create(['registry_number' => '1872', 'name' => 'Admin User 1']);
        Person::create(['registry_number' => '9810', 'name' => 'Master User 1']);
        Person::create(['registry_number' => '7738', 'name' => 'Admin User 2']);
    }
}
