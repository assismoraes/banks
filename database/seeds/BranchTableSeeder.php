<?php

use Illuminate\Database\Seeder;
use App\Models\Branch;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Branch::create(['name' => 'Agência 1 do Banco do Brasil', 'bank_id' => 1, 'number' => '88273', 'user_id' => 2]);
        Branch::create(['name' => 'Agência 1 da Caixa', 'bank_id' => 5, 'number' => '64550', 'user_id' => 3]);
    }
}
