<?php

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::create(['id' => 1, 'description' => 'common_user']);
        UserType::create(['id' => 2, 'description' => 'admin_user']);
        UserType::create(['id' => 3, 'description' => 'master_user']);
    }
}
