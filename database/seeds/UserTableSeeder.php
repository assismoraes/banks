<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'person_id' => 1, 
            'login' => '76263', 
            'password' => Hash::make('123456'), 
            'user_type_id' => 1
            ]);
        User::create([
            'person_id' => 2, 
            'login' => '83998', 
            'password' => Hash::make('123456'), 
            'user_type_id' => 2
            ]);
        User::create([
            'person_id' => 4, 
            'login' => '71625', 
            'password' => Hash::make('123456'), 
            'user_type_id' => 2
            ]);            
        User::create([
            'person_id' => 3, 
            'login' => '52610', 
            'password' => Hash::make('123456'), 
            'user_type_id' => 3
            ]);
                    
    }
}
