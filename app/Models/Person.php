<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $guarded = [];
    
    protected $hidden = [];

    public function users() {
        return $this->hasMany('App\User');
    }
}
    