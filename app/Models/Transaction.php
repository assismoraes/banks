<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Account;

class Transaction extends Model
{
    protected $guarded = [];

    protected $hidden = [
        'updated_at'
    ];

    public function account() {
        return $this->belongsTo(Account::class);
    }
}
