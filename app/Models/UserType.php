<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $guarded = [];

    public function users() {
        return $this->hasMany('App\User');
    }

    public static function findByName($name) {
        return static::where('description', '=', $name)->first();
    }
}
