<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $guarded = [];

    public function branch() {
        return $this->belongsTo('App\Models\Branch')->first();
    }

    public function transactions() {
        return $this->hasMany('App\Models\Transaction');
    }
}
