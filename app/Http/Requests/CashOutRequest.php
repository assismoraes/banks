<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ValidTransactionValueRule;
use App\Rules\SufficientBalanceRule;

use Illuminate\Support\Facades\Input;

class CashOutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currentBalance = Auth()->user()->account()->current_balance;
        return [
            'value' => ['required', 'numeric', new ValidTransactionValueRule, new SufficientBalanceRule($currentBalance)]
        ];
    }

    public function messages() {
        return [
            'value.required' => 'The transaction value is required.',
            'value.numeric' => 'Invalid Transaction value.',
        ];
    }
}
