<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\BankExistsRule;

class EditBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', new BankExistsRule],
            'name' => 'required',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'The name of the bank is required',
            'id.required' => 'The id of the bank is required',
        ];
    }
}
