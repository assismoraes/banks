<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\BranchExistsRule;
use App\Rules\AccountExistsRule;
use App\Rules\BankExistsRule;
use App\Rules\ValidTransactionValueRule;
use App\Rules\SufficientBalanceRule;

use Illuminate\Support\Facades\Input;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $bankId = Input::get('bank_id');
        $branchNumber = Input::get('branch_number');
        $accountNumber = Input::get('account_number');
        $currentBalance = Auth()->user()->account()->current_balance;
        return [
            'bank_id' => ['required', new BankExistsRule],
            'branch_number' => ['required', new BranchExistsRule],
            'account_number' => ['required', new AccountExistsRule($branchNumber, $accountNumber)],
            'value' => ['required', 'numeric', new ValidTransactionValueRule, new SufficientBalanceRule($currentBalance)]
        ];
    }

    public function messages() {
        return [
            'branch_number.required' => 'The branch number is required.',
            'account_number.required' => 'The account number is required.',
            'value.required' => 'The value of transaction is required.',
            'bank_id.required' => 'The id of the bank is required.',
            'value.numeric' => 'Invalid transaction value.'
        ];
    }
}
