<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registry_number' => 'required|unique:people',
            'password' => 'required',
            'login' => 'required|unique:users',
            'name' => 'required'
        ];
    }

    public function messages() {
        return [
            'registry_number.required' => 'The registry number is required',
            'password.required' => 'The password is required',
            'registry_number.unique' => 'The registry number already exists',
            'login.required' => 'The login is required',
            'login.unique' =>  'The login already exists',
            'name.required' => 'The name is required'
        ];
    }
}
