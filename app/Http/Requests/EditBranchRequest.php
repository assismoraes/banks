<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\BranchExistsRule;

class EditBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => ['required', new BranchExistsRule],
            'name' => 'required',
        ];
    }

    public function messages() {
        return [
            'number.required' => 'The number of the branch is required.',
            'name.required' => 'The name of the branch is required.',
        ];
    }
}
