<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\AdminUserExistsRule;

class EditAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'registry_number' => 'required',
            'id' => ['required', new AdminUserExistsRule]      
        ];
    }

    public function messages() {
        return [
            'name.required' => 'The name is required',
            'registry_number.required' => 'The registry number is required',
            'id.required' => 'The id of the admin user is required'
        ];
    }
}
