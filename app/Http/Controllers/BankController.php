<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateBankRequest;
use App\Http\Requests\EditBankRequest;
use App\Models\Bank;

class BankController extends Controller
{
    
    public function create_bank(CreateBankRequest $request) {
        Bank::create(['name' => $request['name']]);        
    }

    public function edit_bank(EditBankRequest $request) {
        $bank = Bank::find($request['id']);
        $bank->name = $request['name'];
        $bank->save();
    }

    public function list_banks() {
        return Bank::all();
    }

    public function delete_bank($id) {
        Bank::destroy($id);
    }

}
