<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateBranchRequest;

use App\Models\Branch;
use App\Models\Bank;
use App\Http\Requests\EditBranchRequest;

class BranchController extends Controller
{
    public function create_branch(CreateBranchRequest $request) {
        $bank = Bank::find($request['bank_id']);
        $bank->branches()->create([
            'number' => (string) $this->generateBranchNumber(), 
            'name' => $request['name']
            ]);
        return 'success';
    }

    public function edit_branch(EditBranchRequest $request) {
        $branch = Branch::where('number', '=', $request['number'])->first();
        $branch->name = $request['name'];
        $branch->save();
        return 'success';
    }

    public function list_branches() {
        return Branch::all();
    }

    public function list_branches_by_bank($bankId) {
        $bank = Bank::find($bankId);
        if($bank == null)
            return response()->json(['message' => 'The bank does not exists.'], 422);
        return $bank->branches()->get();
    }

    public function delete_branch($number) {
        Branch::destroy($number);
        return 'success';
    }
    
    private function generateBranchNumber() {
        $branchNumber = rand(10000, 99999);
        while(Branch::find($branchNumber) != null) {
            $branchNumber = rand(10000, 99999);
        }
        return $branchNumber;        
    }
}
