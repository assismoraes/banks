<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateAccountRequest;
use App\Http\Requests\DepositRequest;
use App\Http\Requests\CashOutRequest;
use App\Http\Requests\TransferRequest;

use App\Models\Account;
use App\User;
use App\Models\Person;
use App\Models\UserType;
use App\Models\Transaction;
use Hash;
use Auth;

class AccountController extends Controller
{
    
    public function create_account(CreateAccountRequest $request) {
        $person = Person::create(['name' => $request['name'], 'registry_number' => $request['registry_number']]);
        $userType = UserType::where('description', '=', 'common_user')->first();
        $accountNumber = (string) $this->generateAccountNumber();
        $branchOfNewAccount = $request->user()->branch();
        $user = User::create([
            'person_id' => $person->id, 
            'user_type_id' => $userType->id, 
            'login' => $branchOfNewAccount->number . $accountNumber,
            'password' => Hash::make($request['password'])
            ]);        
        $account = Account::create([
            'number' => $accountNumber,
            'branch_id' => $branchOfNewAccount->id,
            'user_id' => $user->id
        ]);
        return $account;
    }

    public function deposit_to_any_account(DepositRequest $request) {
        $account = Account::where('number', '=', $request['account_number'])->first();
        $account->current_balance += $request['value'];
        $account->save();
        Transaction::create(['account_id' => $account->id, 'value' => $request['value'  ]]);
        return 'success';
    }

    public function cash_out(CashOutRequest $request) {
        $account = $request->user()->account();
        $account->current_balance -= $request['value'];
        $account->save();
        Transaction::create(['account_id' => $account->id, 'value' => $request['value'  ] * (-1) ]);
        return 'success';
    }

    public function transfer(TransferRequest $request) {
        $accountDebit = $request->user()->account();
        $accountDebit->current_balance -= $request['value'];
        $accountDebit->save();

        $accountCredit = Account::where('number', '=', $request['account_number'])->first();
        $accountCredit->current_balance += $request['value'];
        $accountCredit->save();

        Transaction::create(['account_id' => $accountDebit->id, 'other_account_id' => $accountCredit->id, 'value' => $request['value'  ] * (-1) ]);
        Transaction::create(['account_id' => $accountCredit->id, 'other_account_id' => $accountDebit->id, 'value' => $request['value'  ]]);

        return 'success';
    }

    public function transactions($month, $year) {
        return Auth::user()->account()
                    ->transactions()
                    ->whereYear('created_at', $year)
                    ->whereMonth('created_at', $month)
                    ->get();
    }

    public function current_balance() {
        return Auth::user()->account()->current_balance;
    }

    private function generateAccountNumber() {
        $accountNumber = rand(100000, 999999);
        while(Account::where('number', '=', $accountNumber)->first() != null) {
            $accountNumber = rand(100000, 999999);
        }
        return $accountNumber;        
    }
}
