<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateAdminRequest;
use App\Http\Requests\EditAdminRequest;
use App\Models\UserType;
use App\Models\Person;
use App\User;
use Hash;

class UserController extends Controller
{
    public function create_admin(CreateAdminRequest $request) {
        $userType = UserType::findByName('admin_user');
        $person = Person::create([
            'registry_number' => $request['registry_number'],
            'name' => $request['name'],
        ]);
        $person->users()->save(new User([
            'login' => $request['login'],
            'password' => Hash::make($request['password']),
            'user_type_id' => $userType->id,
        ]));
        return 'success';
    }

    public function edit_admin(EditAdminRequest $request) {
        $adminUser = User::find($request['id']);
        $person = $adminUser->person()->first();
        $person->name = $request['name'];
        $person->registry_number = $request['registry_number'];
        $person->save();
        return 'success';
    }

    public function list_admins() {
        return User::where('user_type_id', '=', UserType::findByName('admin_user')->id)->get();
    }

    public function delete_admin($id) {
        $user = User::find($id);
        $userType = UserType::findByName('admin_user');
        if($user != null && $user->userType()->first()->id == $userType->id)
            User::destroy($id);
        return 'success';
    }    

    public function find_admin($search = '') {
        $userType = UserType::findByName('admin_user');
        $allAdmins = User::where('user_type_id', '=', $userType->id)
                    ->get();
        $ids = [];
        foreach ($allAdmins as $admin) {
            $ids[] = $admin->person()->first()->id;
        }        
        $people = Person::whereIn('id', $ids)
                ->where('registry_number', 'LIKE', '%'. $search .'%')
                ->get();
                
        return $people;
    }
}
