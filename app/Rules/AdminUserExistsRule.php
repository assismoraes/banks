<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;

class AdminUserExistsRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::find($value); 
        if($user == null)
            return false;
        return $user->userType()->first()->description == 'admin_user';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The user does not exists or it is not an admin user';
    }
}
