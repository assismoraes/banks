<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\Models\Branch;
use App\Models\Account;

class AccountExistsRule implements Rule
{
    private $branchNumber = "";
    private $accountNumber = "";
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($branchNumber, $accountNumber)
    {
        $this->branchNumber = $branchNumber;
        $this->accountNumber = $accountNumber;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $account = Account::where('number', '=', $this->accountNumber)->first();
        if($account != null) {
            if($account->branch()->number == $this->branchNumber)
                return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid account or branch number.';
    }
}
