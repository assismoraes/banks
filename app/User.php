<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\Models\UserType;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password', 'login', 'user_type_id', 'person_id'
    ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Change authentication user's field to LOGIN
     */
    public function findForPassport($identifier) {
        return $this->orWhere('login', $identifier)->first();
    }

    public function userType() {
        return $this->belongsTo('App\Models\UserType');
    }

    public function person() {
        return $this->belongsTo('App\Models\Person');
    }

    public function branch() {
        return $this->hasOne('App\Models\Branch')->first();
    }    

    public function account() {
        return $this->hasOne('App\Models\Account')->first();
    }
}
